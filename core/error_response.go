package core

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type ErrorResponse struct {
	ctx     *gin.Context
	code    int
	message string
	errors  Json
}

func (response *ErrorResponse) ToJSON() Json {
	return Json{
		"code":    response.code,
		"message": response.message,
		"errors":  response.errors,
	}
}

func (response *ErrorResponse) Code(code int) *ErrorResponse {
	response.code = code
	return response
}

func (response *ErrorResponse) Message(message string) *ErrorResponse {
	response.message = message
	return response
}

func (response *ErrorResponse) Errors(errors Json) *ErrorResponse {
	response.errors = errors
	return response
}

func (response *ErrorResponse) Dispatch(statusCode int) {
	if response.message == "" {
		response.Message("An Error has occurred")
	}

	response.ctx.JSON(statusCode, response.ToJSON())
}

func (response *ErrorResponse) Validation() {
	response.code = VALIDATION_ERROR

	if response.message == "" {
		response.Message("Validation Failed")
	}

	response.Dispatch(http.StatusUnprocessableEntity)
}
