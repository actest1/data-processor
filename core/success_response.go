package core

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type SuccessResponse struct {
	ctx     *gin.Context
	code    int
	message string
	data    Json
}

func (response *SuccessResponse) ToJSON() Json {
	return Json{
		"code":    response.code,
		"message": response.message,
		"data":    response.data,
	}
}

func (response *SuccessResponse) Code(code int) *SuccessResponse {
	response.code = code
	return response
}

func (response *SuccessResponse) Message(message string) *SuccessResponse {
	response.message = message
	return response
}

func (response *SuccessResponse) Data(data Json) *SuccessResponse {
	response.data = data
	return response
}

func (response *SuccessResponse) Dispatch(statusCode int) {
	response.ctx.JSON(statusCode, response.ToJSON())
}

func (response *SuccessResponse) List() {
	if response.message == "" {
		response.Message("Resources Fetched Successfully")
	}

	response.Dispatch(http.StatusOK)
}

func (response *SuccessResponse) Show() {
	if response.message == "" {
		response.Message("Resource Fetched Successfully")
	}

	response.Dispatch(http.StatusOK)
}

func (response *SuccessResponse) Create() {
	if response.message == "" {
		response.Message("Resource Created Successfully")
	}

	response.Dispatch(http.StatusCreated)
}

func (response *SuccessResponse) Update() {
	if response.message == "" {
		response.Message("Resource Updated Successfully")
	}

	response.Dispatch(http.StatusAccepted)
}

func (response *SuccessResponse) Delete() {
	if response.message == "" {
		response.Message("Resource Deleted Successfully")
	}

	response.Dispatch(http.StatusAccepted)
}
