package core

import "github.com/gin-gonic/gin"

func NewSuccessResponseBuilder(ctx *gin.Context) *SuccessResponse {
	return &SuccessResponse{ctx: ctx, code: SUCCESS, data: Json{}}
}

func NewErrorResponseBuilder(ctx *gin.Context) *ErrorResponse {
	return &ErrorResponse{ctx: ctx, code: FAIL, errors: Json{}}
}
