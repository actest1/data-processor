package main

import (
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	// Setup logger with custom format
	setupLogger()

	// Get Gin Router
	router := gin.Default()

	// Setup routes
	setupRoutes(router)

	// Run Server on Port *:9010
	if runServerError := router.Run(":9010"); runServerError != nil {
		log.Fatalf("An Error Occured While Running Server: %v", runServerError.Error())
	}
}

func setupLogger() {
	log.SetFlags(log.Lmsgprefix | log.LstdFlags)
	log.SetPrefix("Arvan Data Processor: ")
}

func setupRoutes(router *gin.Engine) {
	// Register pre-defined module's routes
}
